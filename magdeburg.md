# Vereinssatzung

## Präambel

An Stellen, an denen schriftliche Kommunikation gefordert wird, sind E-Mail sowie weitere 

geeignete digitale Mittel stets mit eingeschlossen.

# § 1 Name, Sitz, Geschäftsjahr

1. Der Verein führt den Namen "Softwerke Magdeburg".

2. Er soll in das Vereinsregister eingetragen werden. Nach der Eintragung führt er zu seinem 

Namen den Zusatz e. V.

3. Der Verein hat seinen Sitz in Loitsche.

4. Das Geschäftsjahr ist das Kalenderjahr.

# § 2 Zweck des Vereins

Abgabenordnung (AO).

2. Die Zwecke des Vereins sind:

1. Der Verein "Softwerke Magdeburg" mit Sitz in Loitsche verfolgt ausschließlich und 

unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts "Steuerbegünstigte Zwecke" der 

Die Förderung der Nutzung von freien und offenen digitalen Diensten.

Der Austausch mit und die Förderung von Open-Source-Projekten.

Die Aufklärung und Bildung zum Thema freier und offener Software.

Die Bereitstellung von freien digitalen Diensten, insbesondere für Menschen die keinen 

Zugang zu vergleichbaren kommerziellen Lösungen haben.

Die Kooperation mit anderen Organisationen, um die oben genannten Ziele zu 

verwirklichen.

3. Der Verein verwirklicht die Zwecke unmittelbar selbst, sowie als Förderkörperschaft für 

andere steuerbegünstigte Körperschaften.

4. Der Satzungszweck wird insbesondere verwirklicht durch das Anbieten von freien und 

offenen digitalen Diensten, mit Fokus auf den Großraum Magdeburg.

5. Die Mitglieder der Organe des Vereins, sowie mit Aufgaben zur Förderung des Vereins 

betraute Mitglieder, haben gegenüber dem Verein einen Anspruch auf Ersatz der ihnen in 

Zusammenhang mit ihrer Amtsausübung entstandenen Aufwendungen (§ 670 BGB) im 

Rahmen der Beschlüsse des Vorstandes und im Rahmen der finanziellen Leistungsfähigkeit 

des Vereins. Eine Ehrenamtspauschale (§ 3 Nr. 26 a EStG) in Form pauschalen 

Aufwendungsersatzes oder einer Tätigkeitsvergütung kann geleistet werden.

# § 3 Gemeinnützigkeit

1. Der Verein ist selbstlos tätig; er verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.

2. Mittel des Vereins dürfen nur für die satzungsmäßigen Zwecke verwendet werden. Die 

Mitglieder erhalten keine Zuwendungen aus Mitteln der Körperschaft.

3. Es darf keine Person durch Ausgaben, die dem Zweck der Körperschaft fremd sind, oder 

durch unverhältnismäßig hohe Vergütungen begünstigt werden.

# § 4 Mitgliedschaft

1. Der Verein besteht aus aktiven Mitgliedern (§ 4 Abs. 2), Fördermitgliedern (§ 4 Abs. 3) und 

Ehrenmitgliedern (§ 4 Abs. 4), die sich für die Erreichung der Vereinszwecke einsetzen. Die 

Beitrittserklärung erfolgt gemäß § 5.

2. Die aktive Mitgliedschaft kann von jeder natürlichen Person erworben werden, die sich zum 

Vereinszweck bekennt, und durch aktive Mitarbeit einen regelmäßigen Beitrag leistet.

3. Die Fördermitgliedschaft kann von natürlichen und juristischen Personen erworben werden, 

die dem Verein bei der Erreichung seines Vereinszwecks unterstützen wollen.

4. Mitglieder die sich in besonderer Weise um die Arbeit des Vereins verdient gemacht haben, 

können auf Antrag des Vorstands und durch Bestätigung der Mitgliederversammlung zu 

Ehrenmitgliedern ernannt werden.

5. Über den schriftlichen Aufnahmeantrag entscheidet der Vorstand. Will der Vorstand einen 

Aufnahmeantrag ablehnen, so legt er ihn der nächsten ordentlichen Mitgliederversammlung 

vor. Diese entscheidet endgültig.

§ 5 Erwerb und Beendigung der Mitgliedschaft

 

1. Die Mitgliedschaft muss gegenüber dem Vorstand schriftlich beantragt werden. Über den 

Aufnahmeantrag wird gemäß § 4 entschieden. Der Vorstand ist gegenüber der 

antragsstellenden Person nicht verpflichtet, Ablehnungsgründe mitzuteilen.

2. Die Mitgliedschaft beginnt mit der Aushändigung einer schriftlichen Bestätigung durch ein 

Vorstandsmitglied.

3. Die Mitgliedschaft endet

durch freiwilligen Austritt;

durch Streichung von der Mitgliederliste gemäß § 5 Abs. 8;

durch Ausschluss aus dem Verein gemäß § 6;

bei juristischen Personen mit Abschluss der Liquidation;

oder bei natürlichen Personen mit dem Tod des Mitglieds.

4. Der Austritt ist gegenüber dem Vorstand mit einer zweiwöchigen Frist zum Ende des 

Quartals schriftlich zu erklären.

5. Bei Beendigung der Mitgliedschaft, gleich aus welchem Grund, erlöschen alle Ansprüche aus 

dem Mitgliedsverhältnis. Eine Rückgewähr von Beiträgen, Spenden oder sonstigen 

Unterstützungsleistungen ist grundsätzlich ausgeschlossen.

6. Der Anspruch des Vereins auf rückständige Beitragsforderungen bleibt von einem Ende der 

Mitgliedschaft unberührt.

Satzung auszuhändigen.

7. Dem Mitglied ist beim Eintritt in den Verein eine schriftliche oder digitale Kopie dieser 

8. Ein Mitglied kann bei Beitragsrückstand von mehr als zwölf Monaten durch Beschluss des 

Vorstandes von der Mitgliederliste gestrichen werden, wenn es trotz zweimaliger Mahnung 

mit der Zahlung des Beitrages im Rückstand ist. Die Streichung darf erst beschlossen 

werden, nachdem seit der Absendung des zweiten Mahnschreibens drei Monate verstrichen 

und die Beitragsschulden nicht beglichen sind. Die Streichung ist dem Mitglied mitzuteilen.

# § 6 Ausschluss aus dem Verein

 

1. Der Ausschluss eines Mitgliedes mit sofortiger Wirkung und aus wichtigem Grund kann dann 

ausgesprochen werden, wenn das Mitglied in grober Weise dem Zwecke, der Satzung, den 

Zielen oder der Ordnung des Vereins zuwider handelt oder das Ansehen des Vereins in der 

Öffentlichkeit in grober Weise schädigt.

2. Über den Ausschluss entscheidet der Vorstand in einfacher Stimmmehrheit.

3. Dem Mitglied ist unter Fristsetzung von zwei Wochen Gelegenheit zu geben, sich zu den 

Vorwürfen zu äußern. Legt das Mitglied gegen den Ausschluss Widerspruch beim Vorstand 

ein, so entscheidet die Mitgliederversammlung endgültig über den Ausschluss.

# § 7 Rechte und Pflichten der Mitglieder

1. Die aktiven Mitglieder sind berechtigt, an allen angebotenen Veranstaltungen des Vereins 

teilzunehmen. Sie haben darüber hinaus das Recht, gegenüber dem Vorstand und der 

Mitgliederversammlung, Anträge zu stellen und an Abstimmungen teilzunehmen.

2. Fördermitglieder haben das Recht, Vorschläge zu Aktivitäten des Vereins zu machen und 

Informationen über die Verwendung der Förderbeiträge zu erhalten.

3. Die Mitglieder sind verpflichtet, den Verein und Vereinszweck - auch in der Öffentlichkeit - in 

ordnungsgemäßer Weise zu unterstützen.

4. Der Verein erhebt einen Mitgliedsbeitrag, zu dessen Zahlung die Mitglieder verpflichtet sind. 

Näheres regelt die Beitragsordnung des Vereins, die von der Mitgliederversammlung 

beschlossen wird.

# § 8 Organe des Vereins

1. Die Organe des Vereins sind der Vorstand und die Mitgliederversammlung.

2. Es können nur aktive Mitglieder einem Organ des Vereins angehören.

# § 9 Vorstand

sowie der Kassenführung.

1. Der Vorstand im Sinne des § 26 BGB besteht aus dem ersten Vorsitz, dem zweiten Vorsitz 

2. Der Verein wird gerichtlich und außergerichtlich durch zwei Mitglieder des Vorstandes, 

darunter dem ersten oder zweiten Vorsitz, vertreten.

3. Der Vorstand kann zweckgebundene Arbeitsgruppen bilden. Jede Arbeitsgruppe wird durch 

eine Ansprechperson geleitet, die durch den Vorstand bestimmt wird. Arbeitsgruppen bilden 

kein eigenes Vereinsorgan.

# § 10 Zuständigkeiten des Vorstandes

Der Vorstand ist für alle Angelegenheiten des Vereins zuständig, soweit sie nicht durch die 

Satzung oder zusätzliche Vereinsordnungen nach § 15 einem anderen Vereinsorgan zugewiesen 

sind, oder der Vorstand beschließt, sie an eine Arbeitsgruppe zu delegieren.

# § 11 Amtsdauer des Vorstandes

1. Der Vorstand wird von der Mitgliederversammlung für die Dauer von einem Jahr gewählt. 

Die unbegrenzte Wiederwahl ist möglich. Der Vorstand bleibt bis zum Amtsantritt seiner 

2. Jedes Vorstandsmitglied ist einzeln zu wählen. Wählbar sind nur Vereinsmitglieder, nach § 4 

3. Das Amt eines Mitglieds des Vorstands endet durch schriftliche Mitteilung des Rücktritts oder 

mit dem Ausscheiden des Mitglieds aus dem Verein. Endet das Vorstandsamt eines Mitglieds 

aus einem dieser Gründe vor Ablauf der Amtsdauer, bestimmen die verbleibenden 

Mitglieder des Vorstands ein Ersatzmitglied für den Rest der Amtszeit des ausgeschiedenen 

Nachfolge im Amt.

Abs. 2.

Mitglieds.

# § 12 Beschlussfassung des Vorstandes

1. Der Vorstand fasst seine Beschlüsse in Vorstandssitzungen. Diese werden durch den ersten 

Vorsitz, bei dessen Verhinderung durch den zweiten Vorsitz, schriftlich und vereins-öffentlich 

mit einer Frist von 7 Tagen einberufen. Der Mitteilung einer Tagesordnung bedarf es nicht.

2. Der Vorstand entscheidet mit einer einfachen Mehrheit der abgegebenen Stimmen. Die 

Vorstandssitzung ist beschlussfähig, wenn mindestens zwei Vorstandsmitglieder anwesend 

sind. Bei Stimmgleichheit gilt der Beschluss als abgelehnt.

3. Der erste und zweite Vorsitz sind von den Beschränkungen des § 181 BGB befreit.

4. Die Sitzung ist zu protokollieren. Der Vorstand bestimmt zu Beginn jeder Sitzung eine 

schriftführende Person, die das Protokoll anfertigt.

5. Das Protokoll jeder Sitzung ist innerhalb von 7 Tagen nach der Sitzung vereins-öffentlich 

schriftlich bekanntzumachen.

6. Die Vorstandssitzungen sind grundsätzlich vereins-öffentlich.

7. Für Angelegenheiten, die das Diskutieren personenbezogener Daten erfordern, kann der 

Vorstand über den Ausschluss der Öffentlichkeit oder von Teilen der Öffentlichkeit 

8. Pro Quartal ist mindestens eine Vorstandssitzung abzuhalten, sofern dringende Gründe dies 

9. Die Vorstandssitzung wird durch den ersten Vorsitz, in seiner Abwesenheit durch den 

entscheiden.

nicht verhindern.

zweiten Vorsitz, geleitet.

10. Auf den Vorstandssitzungen haben die Vorstandsmitglieder Stimm- und Rederecht. 

Ansprechpersonen der Arbeitsgruppen nach § 9 Abs. 3 haben Rederecht in Angelegenheiten, 

die ihren jeweiligen Zweck betreffen. Der Vorstand kann für jede sonstige anwesende Person 

über das Stimmrecht entscheiden.

11. Bei wiederholter Störung der Sitzung kann der Vorstand beschließen, betreffenden 

Anwesenden von der weiteren Teilnahme an der Sitzung ausschließen.

12. Vorstandsmitglieder und andere Personen können zu jeder Sitzung mit geeigneten digitalen 

Hilfsmitteln, wie Video-Konferenzlösungen, zugeschaltet werden. So zugeschaltete Mitglieder 

können ihr volles Stimmrecht wahrnehmen und gelten als anwesend.

# § 13 Mitgliederversammlung

 

1. Die Mitgliederversammlung ist das oberste Beschlussorgan des Vereins. Ihr obliegen alle 

Entscheidungen, die nicht vom Vorstand getroffen werden dürfen, sowie Entscheidungen, 

die den Vorstand oder seine Mitglieder direkt betreffen.

2. Die Mitgliederversammlung ist grundsätzlich ein Mal pro Geschäftsjahr einzuberufen.

3. In der Regel wird die Mitgliederversammlung durch den Vorstand einberufen.

4. Die Einladung zur Mitgliederversammlung muss spätestens 14 Tage vorher schriftlich durch 

den Vorstand an alle Mitglieder nach § 4 versandt werden.

5. Die Mitgliederversammlung wird durch den Vorstand geleitet.

6. Die Mitgliederversammlung entlastet den Vorstand.

7. Die Mitgliederversammlung kann Änderungen an der Vereinssatzung mit 2/3-Mehrheit 

vornehmen.

8. Die Mitgliederversammlung ist unabhängig von der Anzahl anwesender Mitglieder nach § 4 

Abs. 2 beschlussfähig. Die Anwesenheit kann durch den Einsatz von geeigneten digitalen 

Hilfsmitteln, wie Video-Konferenzlösungen, sichergestellt werden.

9. Anträge an die Tagesordnung sind spätestens 3 Tage vor der Mitgliederversammlung an den 

Vorstand zu stellen.

10. Die Mitgliederversammlung kann zusätzlich einmal pro Quartal auf Verlangen von 1/3 der 

Mitglieder nach § 4 Abs. 2 einberufen werden. Dies kann nur durch die Angabe eines triftigen 

Grundes erfolgen. Das Verlangen ist dem Vorstand schriftlich mitzuteilen. In diesem Fall 

muss die Mitgliederversammlung spätestens 21 Tage nach der Mitteilung stattfinden.

11. Die Mitgliederversammlung kann die Abberufung des Vorstandes fordern, in diesem Fall ist 

eine 2/3-Mehrheit erforderlich. Die Wahl eines neuen Vorstandes erfolgt auf derselben 

Mitgliederversammlung nach § 11 Abs. 1f.

12. Wahlen finden grundsätzlich offen statt, auf Antrag einer wahlberechtigten Person geheim.

# § 14 Kassenprüfung

1. Die Mitgliederversammlung wählt für die Dauer von einem Jahr mindestens zwei Mitglieder 

zur Kassenprüfung. Die Kassenprüfenden dürfen nicht dem Vorstand angehören.

2. Die Kassenprüfenden sind berechtigt, die Kassenführung des Vorstandes laufend zu 

überwachen und dazu die entsprechenden Unterlagen einzusehen. Die Kassenprüfenden 

berichten darüber auf der jährlichen Mitgliederversammlung und entlasten die 

Kassenführung.

# § 15 Ergänzende Dokumente

1. Der Verein gibt sich zur Regelung der vereinsinternen Abläufe Vereinsordnungen. Die 

Vereinsordnungen sind nicht Bestandteil der Satzung.

2. Über den Erlass, die Änderung und Aufhebung von Vereinsordnungen entscheidet die 

Mitgliederversammlung.

# § 16 Mitgliedschaft in anderen Vereinen

1. Der Verein darf Mitglied in anderen Vereinen werden.

2. Die Mitgliederversammlung entscheidet über den Beitritt bzw. Austritt aus anderen 

Vereinen.

# § 17 Salvatorische Klausel

Die Mitgliederversammlung ermächtigt den Vorstand Satzungsänderungen selbstständig 

vorzunehmen, die aufgrund von Moniten des zuständigen Registergerichts oder des Finanzamtes 

notwendig werden und die den Kerngehalt einer zuvor beschlossenen Satzungsänderung nicht 

berühren. Der Vorstand hat die textliche Änderung mit einstimmiger Mehrheit zu beschließen. In 

der auf den Beschluss folgenden Mitgliederversammlung ist diese von der Satzungsänderung in 

Kenntnis zu setzen.

# § 18 Auflösung des Vereins

1. Die Auflösung des Vereins muss von der Mitgliederversammlung mit einer 2/3-Mehrheit 

beschlossen werden.

2. Die Abstimmung ist nur möglich, wenn auf der Einladung zur Mitgliederversammlung als 

einziger Tagesordnungspunkt die Auflösung des Vereins angekündigt wurde.

3. Bei Auflösung des Vereins oder bei Wegfall seiner steuerbegünstigten Zwecke fällt das 

Vereinsvermögen an den Digitalcourage e. V., Marktstraße 18, 33602 Bielefeld, der es 

ausschließlich und unmittelbar zu steuer-begünstigten Zwecken zu verwenden hat.
